#ifndef _01konsola_H
#define _01konsola_H


#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <limits.h>

long int receive();
long int transform(long int number);
void send(const long int number);
void sig_handler();

#endif // _01konsola_H
