#ifndef _03pipe_named_H
#define _03pipe_named_H


#include <stdlib.h>
#include <stdio.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h> 


long int receive();
long int transform(unsigned int number);
void send(const unsigned int number);

#endif // _03pipe_named_H
