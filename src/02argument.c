#include "02argument.h"
#include "00global.h"

// 2. "Argument"
// Wejście: argument do programu, parsowana za pomocą getopt
// Modyfikacja/wyjście: x = Następna liczba pierwsza

int main(int argc, char* argv[])
{
    displayInfo(INTRO, "#####     2     #####   Next prime number");
    char buffer[BUFFER_SIZE];

    long int number = receive(argc , argv); 
    if (!checkRange(number))
    {
        displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
        return 0;
    }
    sprintf(buffer, "%ld", number);
    displayInfo(INPUT, buffer);
    
    number = transform(number);
    sprintf(buffer, "%ld", number);
    displayInfo(OUTPUT, buffer);

    send(number);

    return 0;
}

long int receive(int argc, char* argv[])
{
    int opt;
    char buffer[BUFFER_SIZE] = "\"nothing came to program\"";

    while ((opt = getopt(argc, argv, "i:h")) != -1)
    {
        switch (opt)
        {
        case 'h':
            printf("Help: flag -i accepts value from 01konsola.out\n");
            printf("To run manualy use ./bin/02argument -i value\n");
            printf("where in value put integer number\n");
            break;
        case 'i':
            strcpy(buffer, optarg);
            break;
        }
    }
    for (; optind < argc; optind++)
    {
        printf("extra arguments: %s\n", argv[optind]);
    }
    
    return atol(buffer);
}

long int transform(long int number)
{
    printf("x = next prime(%ld)\n", number);
    bool prime = false;
    while(!prime)
    {
        number++;
        if(!checkRange(number)) return -1;
        if(isPrime(number)) prime=true;
    }
    printf("x = %ld\n", number);
    return number;
}

bool isPrime(const unsigned long number) //trialDivision
{
    if(number <= 1) return false;
    if(number == 2) return true;
    for(unsigned int i = 2; (i*i) <= number; ++i)
    {
        if(number%i == 0) return false;
    }
    return true;
}

void send(const unsigned int number)
{
    int fd;
    mode_t mode = 0666;
    unlink(PIPE_PATH);
    mkfifo(PIPE_PATH, mode);

    pid_t pid = -1;
    if ((pid = fork()) == -1)
    {
        perror("Fork creation failed");
        exit(0);
    }
    else
    {
        if (pid == 0) //Child
        {
            char command[40];
            snprintf(command, 40, "%u", number);
            fd = open(PIPE_PATH, O_WRONLY);
            if ( (write(fd, command, sizeof(int) * 8)) < 0 ) 
            {
                perror("PIPE error on write");
                exit(0);
            }
            close(fd);

            exit(0);
        }
    }

    execl("./bin/03pipe_named.out", "software", NULL);
}
