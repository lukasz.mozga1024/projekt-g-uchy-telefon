#include "01konsola.h"
#include "00global.h"

// 1. "Konsola":
// wejście: stdin
// Modyfikacja/wyjscie: x = x % 10 (reszta z dzielenia)

int main(int argc, char *argv[])
{
    signal(SIGINT, sig_handler);

    displayInfo(INTRO, "#####     1     #####    x = x % 10");
    char buffer[BUFFER_SIZE];
 
    long int number = receive();
    sprintf(buffer, "%ld", number);
    displayInfo(INPUT, buffer);

    number = transform(number);
    sprintf(buffer, "%ld", number);
    displayInfo(OUTPUT, buffer);

    send(number);
  
  return 0;
}

long int receive()
{
    long int number = 0;
    int signs = 0;
    while (signs == 0)
    {
        printf("Please type a nuber in range <0-%u>: ", UINT_MAX);
        signs = scanf("%ld", &number);
        if (signs > 0)
        {
            if (!checkRange(number))
            {
                displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
                continue;
            }
        }
        else
        {
            displayInfo(ERROR, "Incorrect characters");
        }
    }

    return number;
}

long int transform(long int number)
{
    printf("x = %ld %% 10 \n", number);
    number %= 10;
    printf("x = %ld\n", number);
    return number;
}

void send(const long int number)
{
    char command[BUFFER_SIZE];
    sprintf(command, "./bin/02argument.out -i %ld", number);
    system(command);
}

void sig_handler()
{
    putchar('\n');
    puts("Break signal, Exiting program");
    exit (EXIT_SUCCESS);
}