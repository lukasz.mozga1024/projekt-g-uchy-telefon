#ifndef _06network_H
#define _06network_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>

long int receive();
void transform(long int number);
void gt_send(const long int);

#endif // _06network_H