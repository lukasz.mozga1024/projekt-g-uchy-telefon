#ifndef _00global_H
#define _00global_H

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <limits.h> //  w niej zawarty jest UINT_MAX
#include <unistd.h>
#include <signal.h>

#define PIPE_PATH "/tmp/named_pipe"
#define CHRDEV_PATH "/dev/jajko"

#define BUFFER_SIZE 255

// poniższe define zmieniają kolor, czcionki komend
#define CMD_RESET "\x1b[0m"
#define CMD_RED "\x1b[31m"
#define CMD_GREEN "\x1b[32m"
#define CMD_ORANGE "\x1b[33m"
#define CMD_YELLOW "\x1b[38;5;11m"
#define CMD_BG_VIOLET "\x1b[48;5;57m"

#define TXT_ERROR_INPUT_OUT_OF_RANGE "Wejscie jest poza zakresem"
#define TXT_ERROR_OUTPUT_OUT_OF_RANGE "Wyjscie jest poza zakresem"

#define TCP_HOST "localhost"
#define TCP_PORT 7777

typedef enum infoType
{
	ERROR,
	INPUT,
	INTRO,
	OK,
	OUTPUT
} infoType;

bool checkRange(unsigned long int number);
void displayInfo(infoType, char[]);
void displayBits(unsigned int number);


#endif // _00global_H
