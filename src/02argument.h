#ifndef _02argument_H
#define _02argument_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/stat.h> 


long int receive(int argc, char* argv[]);
long int transform(long int number);
bool isPrime(const unsigned long number);
void send(const unsigned int number);


#endif // _02argument_H
