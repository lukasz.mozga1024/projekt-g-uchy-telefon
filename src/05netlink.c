#include "00global.h"
#include "05netlink.h"
#include "genl.h"

// 5. "Netlink"
// Wejście: Generic netlink
// Modyfikacja/wyjscie: suma cyfr liczby x w postaci dziesiętnej

int main()
{
    displayInfo(INTRO, "#####     5     #####   Sum digit by digit");
    char buffer[BUFFER_SIZE];

    long int number = receive(); 

    if(!checkRange(number))
    {
        displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
        return 0;
    }
    sprintf(buffer, "%ld", number);
    displayInfo(INPUT, buffer);

    int sum = transform(number);
    sprintf(buffer, "%d", sum);
    displayInfo(OUTPUT, buffer);

    gt_send(sum);

    return 0;
}

int transform(long int number)
{
    char buff[32];
    itoa(number, buff);
    int sum = 0;
    int i = 0;
    
    /* negative number guard */
    if(buff[0] == ASCII_MINUS)
    {
        displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
        return 0;
    }
    else
    {
        while(buff[i] != '\0')
        {
            sum += (int)buff[i] - '0';
            i++;
        }
        printf("Stacking up digit by digit from nr: %ld\n", number);
        printf("Sum is equal %d\n", sum);

        return sum;
    }
}

void itoa(long int n, char s[])
{
    long int i;
    long int sign;

    if ((sign = n) < 0) /* record sign */
        n = -n;         /* make n positive */
    i = 0;
    do
    {                          /* generate digits in reverse order */
        s[i++] = n % 10 + '0'; /* get next digit */
    } while ((n /= 10) > 0);   /* delete it */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}

void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s) - 1; i < j; i++, j--)
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void gt_send(const unsigned int number)
{
    pid_t pid;
    if ((pid = fork()) == -1)
    {
        perror("Fork error");
        exit(0);
    }
    else
    {
        if (pid == 0)
        {
            inetServer(number);
            exit(0);
        }
    }

    execl("./bin/06network.out", "software", NULL);
}

void inetServer(const unsigned int number)
{
    int server_fd, client_fd;
    int backlog = 5;
    struct sockaddr_in server_addr, client_addr;
    char buffer[BUFFER_SIZE];

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(TCP_PORT); // network byte order

    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd < 0)
    {
        perror("socket");
        exit(0);
    }

    int yes = 1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &yes, sizeof(yes)))
    {
        perror("setsockopt");
        exit(0);
    }
    if (bind(server_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
    {
        perror("bind");
        exit(0);
    }
    if (listen(server_fd, backlog) < 0)
    {
        perror("listen");
        exit(0);
    }

    socklen_t client_addr_len = sizeof(client_addr);
    client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &client_addr_len);
    if (client_fd < 0)
    {
        perror("accept");
        exit(0);
    }

    sprintf(buffer, "%u", number);
    if (send(client_fd, buffer, sizeof(buffer), 0) < 0)
    {
        perror("write");
        exit(0);
    }
    close(client_fd);
    close(server_fd);
}

long int receive()
{
    struct nl_sock *nlsock = NULL;
    struct nl_cb *nlcb = NULL; // callback
    gt_add_group(GT_GENL_MCGRP0);
    gt_genl_prep_sock(&nlsock);
    nlcb = nl_cb_alloc(NL_CB_DEFAULT);
    nl_cb_set(nlcb, NL_CB_SEQ_CHECK, NL_CB_CUSTOM, gt_skip_seq_check, NULL);
    nl_cb_set(nlcb, NL_CB_VALID, NL_CB_CUSTOM, gt_genl_receive_msg, NULL);
    nl_recvmsgs(nlsock, nlcb);
    nl_cb_put(nlcb);
    return atol(gt_get_message());
}


