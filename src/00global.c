#include "00global.h"

//unsigned long UINT_MAX = pow(2,32)-1; ta linijka jest nie potrzebna gdyż  z limits.h mozna uzyskac UINT_MAX ktory 
// zdefiniowany jako zmienna 32 bitowy czyli przyjmuje wartości dodatnie w zakresie od 0 do 4294967295

bool checkRange(unsigned long int number)
{
    if( (number < 0) || (number > UINT_MAX) )
        {
           return false;
        }
    else
        {
           return true;
        }
}

void displayInfo(infoType type, char txt[])
{

	switch(type)
	{
	case ERROR:
		printf("[" CMD_RED "ERROR" CMD_RESET "] %s\n", txt);
		break;

	case INPUT:
		printf("[" CMD_YELLOW "INPUT" CMD_RESET "] %s\n", txt);
		break;

	case INTRO:
		printf(CMD_BG_VIOLET "%s" CMD_RESET "\n", txt);
		break;

	case OK:
		printf("[" CMD_GREEN "OK" CMD_RESET "] %s\n", txt);
		break;

	case OUTPUT:
		printf("[" CMD_ORANGE "OUTPUT" CMD_RESET "] %s\n", txt);
		break;
	}
}

void displayBits(unsigned int number)
{
    unsigned int size = sizeof(number) * 8;
    unsigned mask = 1 << (size - 1);
	unsigned int num = number;
    

    for (unsigned int i = 1; i <= size; i++)
    {
        putchar(number & mask ? '1' : '0');
        number <<= 1;
        if (i % 8 == 0)
        {
            putchar(' ');
        }
    }
	printf(" x = %u", num);
	putchar('\n');
}