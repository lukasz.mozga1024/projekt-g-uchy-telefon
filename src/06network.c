#include "00global.h"
#include "06network.h"

// 6. "Network"
// Wejście: Stream tcp
// Wyjście: stdout na osobnej konsoli -> zmodyfikowana liczba lub info o przekroczeniu zakresu

int main()
{
    displayInfo(INTRO, "#####     6     #####   Output on new console");
    char buffer[BUFFER_SIZE];

    long int number = receive();
	if (!checkRange(number))
	{
		displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
		return 0;
	}
    sprintf(buffer, "%ld", number);
	displayInfo(INPUT, buffer);
    
    transform(number);
    displayInfo(OUTPUT, buffer);
    /* note: to run new command prompt xterm package must be installed
    if not, use: sudo apt install xterm */
    gt_send(number);
    return 0;
}


long int receive()
{
    int client_fd;
    struct sockaddr_in server_addr;
    struct hostent *server;
    char buffer[BUFFER_SIZE] = {0};

    client_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (client_fd < 0)
    {
        perror("socket");
        exit(0);
    }
    server = gethostbyname(TCP_HOST);
    if (server == NULL)
    {
        perror("gethostbyname");
        exit(0);
    }

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(TCP_PORT); // network byte order

    memcpy(&server_addr.sin_addr.s_addr, server->h_addr, server->h_length);

    if (connect(client_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
    {
        perror("connect");
        exit(0);
    }

    if (read(client_fd, buffer, sizeof(buffer)) < 0)
    {
        perror("read");
        exit(0);
    }

    close(client_fd);
    return atol(buffer);
}

void gt_send(const long int number)
{
    pid_t pid;
    if ((pid = fork()) == -1)
    {
        perror("Fork error");
        exit(0);
    }
    else
    {
        if (pid == 0)
        {
            char command[40];
            if (checkRange(number))
            {
                snprintf(command, 40, "%ld", number);
                
            }
            else
            {
                snprintf(command, 40, "%s", TXT_ERROR_OUTPUT_OUT_OF_RANGE);
            }
            execl("/usr/bin/xterm", "xterm", "-hold", "-e", "echo", command, NULL);
        }
    }
}

void transform(long int number)
{
    printf("Sending number = %ld to a secondary console\n", number);
    printf("(new command prompt should appear, if not)\n");
    printf("(make sure you have instaled xterm: sudo apt install xterm)\n");
}