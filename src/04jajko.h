#ifndef _04JAJKO_02_H_
#define _04JAJKO_02_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

long int receive();
void gt_send(const unsigned long int);
unsigned int mirror_bits(unsigned int data);

#endif
