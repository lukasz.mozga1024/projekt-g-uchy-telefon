#include "00global.h"
#include "04jajko.h"
#include "genl.h"
//#include <libiberty/libiberty.h>

//Wejście : interfejs znakowy (linux kernel module over chrdev)
//modyfikacja/wyjście : lustrzane odbicie bitów

int main()
{
	displayInfo(INTRO, "#####     4     #####   Mirror of bits possition");
	char buffer[BUFFER_SIZE];

	long int number = receive();
	sprintf(buffer, "%ld", number);
	displayInfo(INPUT, buffer);
	if (!checkRange(number))
	{
		displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
		return 0;
	}

	number = mirror_bits(number);
	if (!checkRange(number))
	{
		displayInfo(ERROR, TXT_ERROR_OUTPUT_OUT_OF_RANGE);
		return 0;
	}
	sprintf(buffer, "%ld", number);
	displayInfo(OUTPUT, buffer);

	gt_send(number);
	return 0;
}

long int receive()
{
	int fd;
	char buffer[sizeof(int) * 8];
	fd = open(CHRDEV_PATH, O_RDONLY);

	if (fd < 0)
	{
		perror("Nie udało się otworzyć CHRDEV_PATH\n");
		exit(0);
	}
	if (read(fd, buffer, sizeof(int) * 8) < 0)
	{
		perror("Nie mogę odczytać z CHRDEV_PATH\n");
		exit(0);
	}
	close(fd);

	return atol(buffer);
}

void gt_send(const unsigned long int number)
{
	pid_t pid;
	if ((pid = fork()) == -1)
	{
		perror("Błąd fork");
		exit(0);
	}
	else
	{
		if (pid == 0)
		{
			usleep(10000); // czekam na następny krok
			struct nl_sock *nlsock = NULL;
			gt_genl_prep_sock(&nlsock);

			char buffer[BUFFER_SIZE];
			sprintf(buffer, "%ld", number);
			gt_genl_send_msg(nlsock, buffer);

			exit(0);
		}
	}

	execl("./bin/05netlink.out", "software", NULL);
}

unsigned int mirror_bits(unsigned int data)
{
	unsigned int size32 = sizeof(data) * 8;
	unsigned int mirror = 0;
	unsigned int mask = 0;
	unsigned int eval = 0;

	printf("Swaping (mirroring) bits - jajko\n");
	printf("Before: ");
    displayBits(data);
	for (unsigned int i = 1; i <= size32; i++)
	{
		/* seting mask on the first bit of a number */
		mask = data & 1;
		/* shifting youngest bit to the oldest position */
		eval = mask << (size32 - i);
		/* bitwise or result */
		mirror |= eval;
		/* removing youngest bit, (previously shifted to mirror val) */
		data >>= 1;
	}
	printf("After : ");
    displayBits(mirror);
	return mirror;
}
