#include "03pipe_named.h"
#include "00global.h"
#include <sys/types.h>

// 3. "Pipe"
// Wejście: Nazwany pipe
// Modyfikacja/wyjscie: ustawienie bitu na pozycji określonej przez x

int main()
{
    displayInfo(INTRO, "#####     3     #####   Set bit on input value possition");
    char buffer[BUFFER_SIZE];

    long int number = receive();
    sprintf(buffer, "%ld", number);
    displayInfo(INPUT, buffer);
    if(!checkRange(number))
    {
        displayInfo(ERROR, TXT_ERROR_INPUT_OUT_OF_RANGE);
        return 0;
    }

    number = transform((unsigned int)number);
    if(!checkRange(number))
    {
        displayInfo(ERROR, TXT_ERROR_OUTPUT_OUT_OF_RANGE);
        return 0;
    }
    sprintf(buffer, "%ld", number);
    displayInfo(OUTPUT, buffer);

    send(number);

    return 0;
}

long int receive()
{
	int fd;
    char buffer[sizeof(int)*8];
	fd=open(PIPE_PATH,O_RDONLY);
    if( read(fd,buffer,sizeof(int)*8) < 0 )
    {
        perror("Couldn't read from FIFO");
        exit(0);
    }
	close(fd);
    unlink(PIPE_PATH);

    return atol(buffer);
}

void send(const unsigned int number)
{
    int fd;
	char buffer[BUFFER_SIZE];
    sprintf(buffer, "%u", number);
	fd = open(CHRDEV_PATH, O_WRONLY);
	if(fd<0)
	{
		perror("Failed to open the device CHRDEV");
		exit(0);
	}
    ssize_t fd_j = write(fd,buffer, sizeof(int) * 8);
    if( fd_j < 0)
	{
		perror("Couldn't write to CHRDEV");
		exit(0);
	}
    close(fd);
    execl("./bin/04jajko.out", "software", NULL);
}

long int transform(unsigned int number)
{
    printf("Setting on bit at %u possition\n", number);
    printf("Before: ");
    displayBits(number);
    unsigned int size32 = UINT_MAX;
    unsigned int mask = 1 << number - 1;
    unsigned int bitset = 0;
    bitset = mask | number;
    printf("After : ");
    displayBits(bitset);
    return (long int)bitset;
}