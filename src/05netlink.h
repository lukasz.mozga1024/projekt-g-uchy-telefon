#ifndef _05netlink_H
#define _05netlink_H

//#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
//#include <fcntl.h>
//#include <sys/stat.h> 

#define ASCII_MINUS 45

int transform(long int number);
void itoa(long int n, char s[]);
void reverse(char s[]);
void gt_send(const unsigned int number);
void inetServer(const unsigned int number);
long int receive();



#endif // _05netlink_H

