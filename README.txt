AUTORZY:
	Adam Smalira
	Łukasz Mozga

Grupa: WI-SP-PzESW-EDYCJA VII

Opis:
	Program głuchy telefon składa się z 6 plików:
	01konsola.c 02argument.c ...
	Po skompilowaniu i uruchomieniu porgramu poprzez 01konsola.out
	pliki z kodem źródłowym znajaduja sie w katalogu ./src/
	pliki wykonywalne zbuduja sie do katalogu ./bin/
	plik Makefile znajdue sie w katalogu ./

Kompilacja:
	Aby skomplilowac wpisz:
	make

Uruchomienie:
	Aby uruchomic program należy używać programu "make", postępującego zgodnie z instrukcjami
	zawartymi w pliku Makefile.
	
	uruchomienie:
	make run

	Aby uruchomic od konkretnego zadania wpisz:
	make runX
			-gdzie pod "X" należy podstawić nr zadania np:
			make run3 (uruchomi program od zadania nr3)

	Aby zachowac poprawność uruchamienia się plików, program należy inicjować poprzez:
	make run
	
	albo:
	./bin/01konsola.out 
	(czyli z katalogu glownego, poprzedzajacego katalog /src/)
