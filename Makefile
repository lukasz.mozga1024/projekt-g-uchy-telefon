#Makefile
CC = gcc
kJajko = KernelJajko
kGenetlink = KernelGenet
FLAGS1 = -g -pthread -Wpedantic -Wall -std=c11 -Werror 
FLAGS2 = -pedantic -errors
DEST = ./bin/
SORC = ./src/
NET = -I/usr/include/libnl3/ -L/lib/x86_64-linux-gnu/ -lnl-3 -g -Wall -O3 `pkg-config --cflags --libs libnl-genl-3.0`
ASP = -D _GNU_SOURCE -std=c99

main:  debug uninstall chardev 01 02 03 04 05 06 genetlink install
	@echo "Finished"

chardev:
	cd $(kJajko) && $(MAKE) build

01:
	$(CC) $(FLAGS) $(SORC)01konsola.c $(SORC)00global.c -o $(DEST)01konsola.out

02:
	$(CC) $(SORC)02argument.c $(SORC)00global.c -o $(DEST)02argument.out

03:
	$(CC) $(SORC)03pipe_named.c $(SORC)00global.c $(ASP) -o $(DEST)03pipe_named.out

04:
	$(CC) $(SORC)04jajko.c $(SORC)00global.c $(SORC)genl.c $(NET) $(ASP) -o $(DEST)04jajko.out

05:
	$(CC) $(SORC)05netlink.c $(SORC)00global.c $(SORC)genl.c $(NET) $(ASP) -o $(DEST)05netlink.out

06:
	$(CC) $(SORC)06network.c $(SORC)00global.c -o $(DEST)06network.out
#make sure you have instaled xterm: sudo apt install xterm 

genetlink:
	cd $(kGenetlink) && $(MAKE) build

install:
	$(MAKE) -C $(kJajko) install
	$(MAKE) -C $(kGenetlink) install

uninstall:
	$(MAKE) -C $(kJajko) uninstall
	$(MAKE) -C $(kGenetlink) uninstall

clean:
	cd $(kJajko) && $(MAKE) clean
	cd $(kGenetlink) && $(MAKE) clean
	rm -f $(DEST)*.out

debug:
	@echo "Kompiluje"

r:
	@echo "Executing file" 
	sudo ./bin/01konsola.out

r5:
	@echo "Executing file from 05netlink" 
	./bin/05netlink.out

